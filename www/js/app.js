// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
angular.module('bemacash', ['ionic'])
.controller('PagesCtrl', function($scope) {
  $scope.pages = [
    { title: 'Página 1', id:1 },
    { title: 'Página 2', id:2 },
    { title: 'Página 3', id:3 },
    { title: 'Página 4', id:4 },      
    { title: 'Página 5', id:5 },
    { title: 'Página 6', id:6 },
    { title: 'Página 7', id:7 },
    { title: 'Página 8', id:8 },
    { title: 'Vídeo', id:9 },
    { title: 'Inicial', id:10 },
  ];
})


